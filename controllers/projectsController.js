const fs      = require("fs")
const Project = require('../models/project');
const jwt 	    = require('jsonwebtoken');

async function savedImages(userId, projectId, images) {
    let arr = [];
    for (let i = 0; i < images.length; i++) {
        const image     = images[i];
        let fileName    = "/assets/projectsImages/"+jwt.sign({_id: userId, datetime:new Date}, process.env.TOKEN_SECRET)+".png"
        let m           = image.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
        let base64Data  = Buffer.from(m[2],'base64');
        await fs.writeFile("public"+fileName, base64Data, function(err){
            if(!err){
                arr.push(fileName)
                console.log("file Saved")
                console.log(arr)
            }else{
                console.error(err)
            }
        });
    }
    project = await Project.findOne({_id: projectId}, function (err) {
        if (err)
            res.json(err);
    });
    project.images = arr
    project.save()
}

exports.index = function (req, res) {
    let parameters = {}
    if(req.body.length){
        parameters = req.body 
    }
    Project.find(parameters, function (err, projects) {
        if (err)
            res.json(err);
        res.send(projects);
    });
};
exports.create = function (req, res) {
    let images = [];
    var project = new Project();
    project.userId          = req.user._id
    project.name            = req.body.name
    project.brand           = req.body.brand
    project.modelNo         = req.body.modelNo
    project.description     = req.body.description
    project.bidHistory      = []
    project.images          = images
    project.save(function (err, project) {
        if (err)
            res.json(err);
        // if(req.body.images.length){
        //     savedImages(req.user._id, project._id, req.body.images)
        // }
        res.json({
            message: "Project has been created, it is now in reveiw process!",
            data: project
        });
    });
};
exports.update = async function (req, res) {
    const savedImagesPromise = new Promise(async function (resolve, reject) {
        let images = []
        for (let i = 0; i < req.body.images.length; i++) {
            const image     = req.body.images[i];
            let fileName    = "/assets/projectsImages/"+jwt.sign({_id: req.user._id, datetime:new Date}, process.env.TOKEN_SECRET)+".png"
            let m           = image.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
            let base64Data  = Buffer.from(m[2],'base64');
            await fs.writeFile("public"+fileName, base64Data, function(err){
                if(!err){
                    images.push(fileName)
                    console.log("file Saved")
                }else{
                    console.error(err)
                    reject(err)
                }
            });
            if(i+1 == req.body.images.length){
                resolve(images);
            }
        }
    });
    savedImagesPromise.then(async function (images) {
        project = await Project.findOne({_id: req.body.projectId}, function (err) {
            if (err)
                res.json(err);
        });
        project.name            = req.body.name
        project.brand           = req.body.brand
        project.modelNo         = req.body.modelNo
        project.description     = req.body.description
        project.auctionExpireAt = req.body.auctionExpireAt
        project.startingPrice   = req.body.startingPrice
        project.status          = req.body.status
        project.images          = images
        project.save(function (err, project) {
            if (err)
                res.json(err);
            if(req.body.images.length){
            }
            res.json({
                message: "Project has been Updated!",
                data: project
            });
        });
    }).catch(function (error) {  
        console.error(error)
    })
};
exports.delete = async function (req, res) {
    try {
        await Project.findByIdAndRemove(req.body.projectId)
        res.json({
            message: "Project has been deleted!",
        });        
    } catch (error) {
        res.json({
            message: error,
            error: true
        });        
    }

};
exports.addbids = async function (req, res) {
    project = await Project.findOne({_id: req.body.projectId}, function (err) {
        if (err)
            res.json(err);
    });
    project.bidHistory.push({
        userId      : req.user._id,
        bidAmount   : req.body.bidAmount,
        bidTime     :  new Date(),
        status      : "pending",
    })
    project.save(function (err) {
        if (err)
            res.json(err);
        res.json({
            message: "Bid has been submitted to this Project!",
            data: project
        });
    });
};