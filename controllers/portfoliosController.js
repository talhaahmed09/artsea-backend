const fs        = require("fs")
const Portfolio = require('../models/portfolio');
const jwt       = require('jsonwebtoken');

exports.index = function (req, res) {
    let parameters = {}
    if(req.body.length){
        parameters = req.body 
    }
    Portfolio.find(parameters, function (err, portfolio) {
        if (err)
            res.json(err);
        res.send(portfolio);
    });
};
exports.create = function (req, res) {
    const savedFilesPromise = new Promise(async function (resolve, reject) {
        let files = []
        for (let i = 0; i < req.body.files.length; i++) {
            const file     = req.body.files[i];
            let fileName    = "/assets/portfoliosFiles/"+jwt.sign({_id: req.user._id, datetime:new Date}, process.env.TOKEN_SECRET)+".png"
            let m           = file.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
            let base64Data  = Buffer.from(m[2],'base64');
            await fs.writeFile("public"+fileName, base64Data, function(err){
                if(!err){
                    files.push(fileName)
                }else{
                    console.error(err)
                    reject(err)
                }
            });
        }
        resolve(files);
    });
    savedFilesPromise.then(async function (files) {
        portfolio               = new Portfolio();
        portfolio.userId        = req.body.userId
        portfolio.title         = req.body.title
        portfolio.description   = req.body.description
        portfolio.save(function (err, portfolio) {
            if (err)
                res.json(err);
            portfolio.files     = files
            console.log(files)
            portfolio.save()
            res.json({
                message: "Portfolio has been Created!",
                data: portfolio
            });
        });
    }).catch(function (error) {  
        console.error(error)
    })
};
exports.update = async function (req, res) {
    const savedFilesPromise = new Promise(async function (resolve, reject) {
        let files = []
        for (let i = 0; i < req.body.files.length; i++) {
            const file     = req.body.files[i];
            let fileName    = "/assets/portfoliosFiles/"+jwt.sign({_id: req.user._id, datetime:new Date}, process.env.TOKEN_SECRET)+".png"
            let m           = file.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);
            let base64Data  = Buffer.from(m[2],'base64');
            await fs.writeFile("public"+fileName, base64Data, function(err){
                if(!err){
                    files.push(fileName)
                    console.log("file Saved")
                }else{
                    console.error(err)
                    reject(err)
                }
            });
            if(i+1 == req.body.files.length){
                resolve(files);
            }
        }
    });
    savedFilesPromise.then(async function (files) {
        portfolio = await Portfolio.findOne({_id: req.body.portfolioId}, function (err) {
            if (err)
                res.json(err);
        });
        portfolio.userId        = req.body.userId
        portfolio.title         = req.body.title
        portfolio.files         = portfolio.files.push(files)
        portfolio.description   = req.body.description
        portfolio.save(function (err, portfolio) {
            if (err)
                res.json(err);
            res.json({
                message: "Portfolio has been Updated!",
                data: portfolio
            });
        });
    }).catch(function (error) {  
        console.error(error)
    })
};
exports.delete = async function (req, res) {
    try {
        await Portfolio.findByIdAndRemove(req.body.portfolioId)
        res.json({
            message: "Portfolio has been deleted!",
        });        
    } catch (error) {
        res.json({
            message: error,
            error: true
        });        
    }

};