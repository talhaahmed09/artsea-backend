const User = require('../models/user');

exports.index = function (req, res) {
    let parameters = {}
    if(req.body.length){
        parameters = req.body 
    }
    User.find(parameters, function (err, users) {
        if (err)
            res.json(err);
        res.send(users);
    });
};
exports.create = async function (req, res) {
    var user = new User();
    user.firstName  = req.body.firstName
    user.lastName   = req.body.lastName
    user.email      = req.body.email.toLowerCase()
    user.password   = req.body.password
    user.mobileNo   = req.body.mobileNo
    if(req.body.userType!=undefined){
        user.userType   = req.body.userType 
    }
    try {
        await user.save(function (err, user) {
            if (err)
                res.json(err);
            res.json({
                message: "New User Created!",
                data: user
            });
        });        
    } catch (error) {
        console.log(error)
    }

};
exports.update = function (req, res) {
    User.findOne({_id: req.user._id}, function (err, users) {
        if (err)
            res.json(err);
        res.send(users);
    });
    
};
exports.delete = function (req, res) {
    
};
exports.updatePassword = function (req, res) {
    
};
exports.addToWatchList = async function (req, res) {
    let user = await User.findOne({_id: req.user._id}, function (err, users) {
        if (err)
            res.json(err);
    });
    if(user.watchLists.length ==0){
        user.watchLists = []
    }
    user.watchLists.push(req.body.productId)
    await user.save(function (err, user) {
        if (err)
            res.json(err);
        res.json({
            message: "Added to Watchlist",
            watchLists: user
        });
    })
};