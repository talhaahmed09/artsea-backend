var express 					= require('express');
var router 						= express.Router();
var usersController 	        = require('../controllers/usersController')
var authenticationController 	= require('../controllers/authenticationController')

router.route('/')				.get(authenticationController.index)
router.route('/authentication')	.post(authenticationController.authentication)
router.route('/userSignUp')	    .post(usersController.create)

module.exports = router;
