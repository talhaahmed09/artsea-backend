const mongoose  = require('mongoose');
const Schema    = mongoose.Schema;

const PortfolioSchema = new Schema({
    userId:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'user',
    },
    title:{ 
        type: String, 
        required: false,
    },
    description:{ 
        type: String, 
        required: false,
    },
    files: {
        type: Array,
        required: false, 
        default: []
    },
    likes: {
        type: Array,
        required: false, 
        default: []
    },
    created_at  : { type: Date, default: Date.now },
    updated_at  : { type: Date, default: Date.now },
    updated     : { type: Date, default: Date.now}
}, {versionKey: false}, {strict: false})


const PortfolioModel = mongoose.model('portfolio', PortfolioSchema);

module.exports  = PortfolioModel;